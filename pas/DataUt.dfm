object DataModule1: TDataModule1
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 364
  Width = 421
  object BaseConnection: TFDConnection
    Params.Strings = (
      'Database=D:\Delphi10\Space2\myplan2\Win32\Debug\cenPlan.sdb'
      'LockingMode=Normal'
      'DriverID=SQLite')
    LoginPrompt = False
    Left = 56
    Top = 24
  end
  object fdquery1: TFDQuery
    Connection = BaseConnection
    SQL.Strings = (
      'drop table Plan;'
      'create table if not exists test('
      '  id int primary key not null,'
      '  name text not null,'
      '  age int not null'
      ');'
      'create table if not exists Plan('
      '  id integer primary key AUTOINCREMENT,'
      '  name text not null,'
      '  classify int not null,'
      '  money real not null,'
      '  bz text not null,'
      '  del int not null,'
      '  create_time text not null,'
      '  plan_time  text not null'
      ');')
    Left = 24
    Top = 128
  end
  object tableInit: TFDQuery
    Connection = BaseConnection
    SQL.Strings = (
      'CREATE TABLE if not exists Plan ('
      '    id          INTEGER  PRIMARY KEY AUTOINCREMENT,'
      '    classify    INT      NOT NULL,'
      '    outin       INT      NOT NULL,'
      '    money       DOUBLE     NOT NULL,'
      '    bz          TEXT     NOT NULL,'
      '    del         INT      NOT NULL,'
      '    create_time DATETIME,'
      '    plan_time   DATETIME,'
      '    msid INTEGER Default 0'
      ');'
      'CREATE TABLE if not exists MonthsSetting ('
      '    id          INTEGER  PRIMARY KEY AUTOINCREMENT,'
      '    year        INT     NOT NULL,'
      '    month       INT      NOT NULL,'
      '    budget      DOUBLE   NOT NULL,'
      '    income      DOUBLE   NOT NULL,'
      '    expend      DOUBLE   NOT NULL,'
      '    bz          TEXT     NOT NULL,'
      '    del         INT      NOT NULL,'
      '    status      INT      NOT NULL,'
      '    create_time DATETIME,'
      '    plan_time   DATETIME'
      ');'
      'CREATE TABLE if not exists Users ('
      '    id          INTEGER PRIMARY KEY AUTOINCREMENT,'
      '    name        TEXT    NOT NULL,'
      '    password    TEXT    NOT NULL,'
      '    wxid        TEXT    NOT NULL,'
      '    zfbid       TEXT    NOT NULL,'
      '    del         INT     NOT NULL,'
      '    create_time TEXT    NOT NULL,'
      '    plan_time   TEXT    NOT NULL'
      ');'
      'CREATE TABLE if not exists Dictionary ('
      '    id          INTEGER PRIMARY KEY AUTOINCREMENT,'
      '    code_cate     TEXT    NOT NULL,'
      '    code          INT     NOT NULL,'
      '    code_name     TEXT    NOT NULL,'
      '    remark        TEXT    NOT NULL'
      ');')
    Left = 200
    Top = 112
  end
  object planfdquery: TFDQuery
    Connection = BaseConnection
    SQL.Strings = (
      'select * from Plan')
    Left = 120
    Top = 192
    object planfdqueryid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object planfdqueryclassify: TIntegerField
      FieldName = 'classify'
      Origin = 'classify'
      Required = True
    end
    object planfdqueryoutin: TIntegerField
      FieldName = 'outin'
      Origin = 'outin'
      Required = True
      OnGetText = planfdqueryoutinGetText
      OnSetText = planfdqueryoutinSetText
    end
    object planfdquerymoney: TFloatField
      FieldName = 'money'
      Origin = 'money'
      Required = True
    end
    object planfdquerybz: TWideMemoField
      FieldName = 'bz'
      Origin = 'bz'
      Required = True
      BlobType = ftWideMemo
    end
    object planfdquerydel: TIntegerField
      FieldName = 'del'
      Origin = 'del'
      Required = True
    end
    object planfdquerycreate_time: TDateTimeField
      FieldName = 'create_time'
      Origin = 'create_time'
    end
    object planfdqueryplan_time: TDateTimeField
      FieldName = 'plan_time'
      Origin = 'plan_time'
    end
    object planfdquerymsid: TIntegerField
      FieldName = 'msid'
      Origin = 'msid'
    end
  end
  object msfdquery: TFDQuery
    Connection = BaseConnection
    SQL.Strings = (
      'select * from MonthsSetting')
    Left = 192
    Top = 192
    object msfdqueryid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object msfdqueryyear: TIntegerField
      FieldName = 'year'
      Origin = 'year'
      Required = True
    end
    object msfdquerymonth: TIntegerField
      FieldName = 'month'
      Origin = 'month'
      Required = True
    end
    object msfdquerybudget: TFloatField
      FieldName = 'budget'
      Origin = 'budget'
      Required = True
    end
    object msfdqueryincome: TFloatField
      FieldName = 'income'
      Origin = 'income'
      Required = True
    end
    object msfdqueryexpend: TFloatField
      FieldName = 'expend'
      Origin = 'expend'
      Required = True
    end
    object msfdquerybz: TWideMemoField
      FieldName = 'bz'
      Origin = 'bz'
      Required = True
      BlobType = ftWideMemo
    end
    object msfdquerydel: TIntegerField
      FieldName = 'del'
      Origin = 'del'
      Required = True
    end
    object msfdquerystatus: TIntegerField
      FieldName = 'status'
      Origin = 'status'
      Required = True
    end
    object msfdquerycreate_time: TDateTimeField
      FieldName = 'create_time'
      Origin = 'create_time'
    end
    object msfdqueryplan_time: TDateTimeField
      FieldName = 'plan_time'
      Origin = 'plan_time'
    end
  end
  object dictionaryfdquery: TFDQuery
    Connection = BaseConnection
    SQL.Strings = (
      'select * from Dictionary')
    Left = 272
    Top = 192
    object dictionaryfdqueryid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object dictionaryfdquerycode_cate: TWideMemoField
      FieldName = 'code_cate'
      Origin = 'code_cate'
      Required = True
      BlobType = ftWideMemo
    end
    object dictionaryfdquerycode: TIntegerField
      FieldName = 'code'
      Origin = 'code'
      Required = True
    end
    object dictionaryfdquerycode_name: TWideMemoField
      FieldName = 'code_name'
      Origin = 'code_name'
      Required = True
      BlobType = ftWideMemo
    end
    object dictionaryfdqueryremark: TWideMemoField
      FieldName = 'remark'
      Origin = 'remark'
      Required = True
      BlobType = ftWideMemo
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 104
    Top = 304
  end
end
