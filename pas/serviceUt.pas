unit serviceUt;
//  业务逻辑接口 分层 与Datamodule 数据层区分
// 20200327 1556 cen
interface
uses Entity,DataUt,Generics.Collections,System.SysUtils,System.DateUtils;

// 按年 月 日 参数 获取 统计数据
function getDataByTypes(year: integer; month: integer):Tlist<RTListAItemData>;
//按天数统计 某一个月
function getDataByMonth(year: integer; month: integer) : TList<TPlan>;
//获取参数时间类所有plan
function getPlanByDate(startD: String; endD: String): TList<TPlan>;
//获取今日支出
function getToDay(checked: boolean): Double;

//获取本月支出，本月收入
function getToMonth(Out expend:double;Out income:double):Boolean;
//function getToMonthExpend(checked: boolean): Double;
//function getToMonthIncome(checked: boolean): Double;

//获取本月预算
function getToMonthBudget(checked: boolean): Double;

//获取msid
function getmsid(nowD: TDateTime): Integer;

//新增一条Plan 数据
function addPlan(dPlan: TPlan): Boolean;

//获取字典中的值
function getDic(code: Integer; codecate: String): String;


implementation
function getDataByMonth(year: integer; month: integer) : TList<TPlan>;
var rPlans: TList<TPlan>;
    rPlan: TPlan;
    sqlStr: String;
begin
   rPlans := TList<TPlan>.create;
   sqlStr :=  'select p.* from Plan p ,MonthsSetting m where p.del = 1 and m.month = %0:D and m.year = %1:D';
   sqlStr := Format(sqlStr,[month,year]);
   if DataModule1.execSql(sqlStr) then
   begin
     with DataModule1.fdquery1 do
     begin
       Close;
       SQL.Clear;
       SQL.Text := sqlStr;
       Open;
       if RecordCount <= 0 then Exit;

       First;
       while not Eof do
       begin
          rPlan := TPlan.Create;
          rplan.id := FieldByName('id').AsInteger;
          rplan.classify := FieldByName('classify').AsInteger;
          rplan.outin := FieldByName('outin').AsInteger;
          rplan.bz := FieldByName('bz').AsString;
          rplan.money := FieldByName('money').AsFloat;
          rplan.del := FieldByName('del').AsInteger;
          rplan.create_time := FieldByName('create_time').AsDateTime;
          rplan.plan_time := FieldByName('plan_time').AsDateTime;
          rplan.msid := FieldByName('msid').AsInteger;
          rplans.Add(rplan);
          Next;
       end;
     end;

   end;

   Result := rPlans;
end;

function getDic(code: Integer; codecate: String): String;
var I: Integer;
    resultStr : String;
begin
   resultStr := '未知';
  for  I := 0 to dicList.Count -1  do
  begin
    if (code = dicList[I].code) And (codecate = dicList[I].code_cate) then
    begin
      resultStr := dicList[I].code_name; Break ;
    end;
  end;

  Result := resultStr;
end;

function getPlanByDate(startD: String; endD: String): TList<TPlan>;
var sqlStr: String;
    plans: TList<TPlan>;
     plan: TPlan;
begin
  //todo 核对参数

  sqlStr := 'select * from Plan where del = 1 And plan_time between :p1  And :p2';
  DataModule1.writeLog(sqlStr+',['+startD+','+endD+']');
  plans := TList<TPlan>.Create;
  with DataModule1.planfdquery do
  begin
    Close;
    SQL.Clear;
    SQL.Text := sqlStr;
    Params[0].Values[0] := startD;
    Params[1].Values[0] := endD;
    Open;
    if RecordCount > 0 then
    begin

      First;
      while not Eof do
      begin
        plan := TPlan.Create;
        plan.id := FieldByName('id').AsInteger;
        plan.classify := FieldByName('classify').AsInteger;
        plan.outin := FieldByName('outin').AsInteger;
        plan.bz := FieldByName('bz').AsString;
        plan.money := FieldByName('money').AsFloat;
        plan.del := FieldByName('del').AsInteger;
        plan.create_time := FieldByName('create_time').AsDateTime;
        plan.plan_time := FieldByName('plan_time').AsDateTime;
        plan.msid := FieldByName('msid').AsInteger;
        plans.Add(plan);
        Next;
      end;
    end;
  end;

  Result := plans;
  //plans.Free;
end;

function addPlan(dPlan: TPlan): Boolean;
var sqlStr: String;
begin
   sqlStr := 'insert into Plan(classify,outin,bz,money,del,create_time,plan_time,msid)'+
   ' Values(:p1,:p2,:p3,:p4,:p5,:p6,:p7,:p8)';
   with DataModule1.planfdquery do
   begin
     Close;
     SQL.Clear;
     SQl.Text := sqlStr;
     Params[0].Values[0] := dPlan.classify;
     Params[1].Values[0] := dPlan.outin;
     Params[2].Values[0] := dPlan.bz;
     Params[3].Values[0] := dPlan.money;
     Params[4].Values[0] := dPlan.del;
     Params[5].Values[0] := dPlan.create_time;
     Params[6].Values[0] := dPlan.plan_time;
     Params[7].Values[0] := dPlan.msid;
     DataModule1.writeLog(SQL.Text);
     Execute;

     if RowsAffected > 0 then
     begin
       Result := True;
     end
     else
     begin
       Result := False;
     end;
   end;

end;


function getmsid(nowD: TDateTime): Integer;
var sqlStr,toyear,tomonth: String;
begin
   sqlStr := 'select id from MonthsSetting where del = 1 and year = ''%0:S'' and month = ''%1:S''';
   toyear := FormatDateTime('yyyy',nowD);
   tomonth := FormatDateTime('mm',nowD);
   sqlStr := Format(sqlStr,[toyear,tomonth]);
   if DataModule1.execSql(sqlStr) then
   begin
     Result := DataModule1.fdquery1.FieldByName('id').AsInteger;
   end
   else
   begin
     Result := 0;
   end;
end;
function getToMonthBudget(checked: boolean): Double;
var sqlStr,toyear,tomonth: String;
begin
   toyear := FormatDateTime('yyyy',now);
   tomonth := FormatDateTime('mm',now);
   sqlStr := 'select * from MonthsSetting where del=1 and year=''%0:S'' and month=''%1:S''';
   sqlStr := Format(sqlStr,[toyear,tomonth]);
   if (DataModule1.execSql(sqlStr)) And (DataModule1.fdquery1.RecordCount>0) then
   begin
      Result := DataModule1.fdquery1.FieldByName('budget').AsFloat;
   end
   else
   begin
     Result :=  0 ;
   end;
end;
function getToMonth(Out expend:double;Out income:double):Boolean;
var sqlStr: String;
    startDay,endDay: string;
begin
    expend := 0;
    income := 0;
    startDay := FormatDateTime('yyyy-mm-dd HH:mm:ss',StartOfTheMonth(Now));
    endDay := FormatDateTime('yyyy-mm-dd HH:mm:ss',EndOfTheMonth(Now));
    sqlStr := 'select outin,sum(money) as m from Plan where plan_time between ''%0:S'' and ''%1:S'' group by outin';
    sqlStr := Format(sqlStr,[startDay,endDay]);
    if DataModule1.execSql(sqlStr) then
    begin
      with DataModule1.fdquery1 do
      begin
         First;
         while not Eof do
         begin
           case FieldByName('outin').AsInteger of
             1: begin
               expend := FieldByName('m').AsFloat;
             end;
             2: begin
               income := FieldByName('m').AsFloat;
             end;
           end;
           Next;
         end;
      end;
    end;
    Result := True;
end;



function getToDay(checked: boolean): Double;
var sqlStr: string;
    today,startTime,endTime:String;
begin
   today := FormatDateTime('yyyy-mm-dd',now);
   startTime := today + ' 00:00:00';
   endTime   := toDay + ' 23:59:59';
   sqlStr := 'select IFNULL(sum(money),0) as todayPay from Plan where outin=1 and del=1 and plan_time between ''%0:S'' and ''%1:S''';
   sqlStr := Format(sqlStr,[startTime,endTime]);
   if DataModule1.execSql(sqlStr) then
   begin
      if DataModule1.fdquery1.FieldByName('todayPay') = nil  then
      begin
        Result := 0 ;
        Exit;
      end;
     if not SameText('',DataModule1.fdquery1.FieldByName('todayPay').Value)  then
     begin
       Result := DataModule1.fdquery1.FieldByName('todayPay').AsFloat;
     end
     else
     begin
       Result := 0 ;
     end;
   end;
end;

function getDataByTypes(year: integer; month: integer):Tlist<RTListAItemData>;
var sqlStr: string;
    rtlist: TList<RTDataByTypes>;
    trData: RTDataByTypes;
    itemData : RTListAItemData;
    itemDatas :TList<RTListAItemData>;
begin
   itemDatas := TList<RTListAItemData>.Create;
   //按年统计
   if (year<=0) And (month <= 0)  then
   begin
       sqlStr := 'select strftime(''%Y'',plan_time) as year ,sum(money) as m,outin,count(1) as num from Plan '+
                 'where del = 1 group by  strftime(''%Y'',plan_time),outin';
       if DataModule1.execSql(sqlStr) then
       begin
       with DataModule1.fdquery1 do
       begin
        if RecordCount>0 then
        begin
           //New(trData);
          // rtlist := TList<RTDataByTypes>.Create;
           First;
           while not Eof do
           begin
//              trData.year := FieldByName('year').AsString;
//              trData.ounin := FieldByName('outin').AsInteger;
//              trData.money := FieldByName('m').AsFloat;
//              rtlist.Add(trData);
                New(itemData);
                itemData.data_id := 1;
                itemData.data_0 := FieldByName('year').AsString;
                itemData.data_money := FieldByName('m').AsFloat;
                itemData.data_outin := FieldByName('outin').AsInteger;
                itemData.data_num := FieldByName('num').AsInteger;
                itemDatas.Add(itemData);
                Next;
           end;

        end;
       end;
     end;
   end;

   //按月份统计  某一年
   if(year>0) And (month<=0) then
   begin
      sqlStr := 'select m.month,IFNULL(sum(p.money),0) as m ,IFNULL(p.outin,0) as outin,count(1) as num from MonthsSetting m LEFT JOIN'
      +' Plan p on m.id = p.msid Where m.year = %0:D GROUP BY m.year,m.month,p.outin ORDER BY m.month';
      sqlStr := Format(sqlStr,[year]);
      if DataModule1.execSql(sqlStr) then
      begin
        with DataModule1.fdquery1 do
        begin
          First;
          //New(trData);
          //rtlist := TList<RTDataByTypes>.Create;
          while not Eof do
          begin

//              trData.year := FieldByName('year').AsString;
//              trData.month := FieldByName('month').AsInteger;
//              trData.ounin := FieldByName('outin').AsInteger;
//              trData.money := FieldByName('m').AsFloat;
//              trData.ounin := FieldByName('outin').AsInteger;
//              rtlist.Add(trData);
                New(itemData);
                itemData.data_id := 2;
                itemData.data_0 := FieldByName('month').AsString;
                itemData.data_money := FieldByName('m').AsFloat;
                itemData.data_outin := FieldByName('outin').AsInteger;
                itemData.data_num := FieldByName('num').AsInteger;
                itemDatas.Add(itemData);
                Next;
          end;
        end;
      end;
   end;

   //按天数统计 某一个月
   if (year > 0 ) and (month > 0) then
   begin
     // sqlStr := 'select * from Plan,MonthsSetting m where p.del = 1 and m.month = %0:D and m.year = %1:D';
      sqlStr := 'select strftime(''%d'',p.plan_time) as day,sum(p.money) as m,p.outin,count(1) as num from Plan p '+
       ',MonthsSetting m where p.del = 1 and m.year = :p1 and m.month = :p2 group by  strftime(''%d'',p.plan_time),p.outin';
     // sqlStr := Format(sqlStr,[year,month]);

     with DataModule1.fdquery1 do
     begin
        Close;
        SQL.Clear;
        SQL.Text := sqlStr;
        Params[0].Values[0] := year;
        Params[1].Values[0] := month;
        Open;
       // if RecordCount <= 0 then Exit;
          First;
         // rtlist := TList<RTDataByTypes>.Create;
         //FieldByName('day').AsString
       // if  FieldByName('day').IsNull then   Exit;

          while not Eof do
          begin
             // New(trData);
             // DataModule1.writeLog(FieldByName('day').AsString);
//              trData.year := year.ToString;
//              trData.month := month;
//              trData.ounin := FieldByName('outin').AsInteger;
//              trData.money := FieldByName('m').AsFloat;
//              trData.day := FieldByName('day').AsInteger;
//              rtlist.Add(trData);
              New(itemData);
              itemData.data_id := 3;
              itemData.data_0 := FieldByName('day').AsString;
              itemData.data_money := FieldByName('m').AsFloat;
              itemData.data_outin := FieldByName('outin').AsInteger;
              itemData.data_num := FieldByName('num').AsInteger;
              itemDatas.Add(itemData);
              Next;
          end;
     end;
//      if DataModule1.execSql(sqlStr) then
//      begin
//        with DataModule1.fdquery1 do
//        begin
//          First;
//          rtlist := TList<RTDataByTypes>.Create;
//          while not Eof do
//          begin
//              trData.year := year.ToString;
//              trData.month := month;
//              trData.ounin := FieldByName('outin').AsInteger;
//              trData.money := FieldByName('m').AsFloat;
//              trData.day := FieldByName('day').AsInteger;
//              rtlist.Add(trData);
//
//              Next;
//          end;
//        end;
//      end;
   end;
   Result := itemDatas;
end;

end.
