unit Entity;
     // 数据实体类
     // 2020/03/27 00:38

interface
//        id          INTEGER  PRIMARY KEY AUTOINCREMENT,
//    year        TEXT     NOT NULL,
//    month       INT      NOT NULL,
//    budget      DOUBLE   NOT NULL,
//    income      DOUBLE   NOT NULL,
//    expend      DOUBLE   NOT NULL,
//    bz          TEXT     NOT NULL,
//    del         INT      NOT NULL,
//    status      INT      NOT NULL,
//    create_time DATETIME,
//    plan_time   DATETIME
uses FireDAC.Comp.Client;


type
  RTDataByTypes = ^TDataByTypes;
  TDataByTypes = Record
    year: String;
    month: Integer;
    day: Integer;
    ounin: byte;
    money: Double;
    classify: byte;
    bz: byte;
    del: byte;
  End;

  RTListAItemData = ^TListAItemData;
  TListAItemData = Record
     data_id : Integer;
     data_0 : String;
     data_money : Double;
     data_outin : Integer;
     data_num : Integer;
  End;
  TMonthsSetting = class
  public
     id: Integer;
     year: Integer;
     month: Integer;
     budget: Double;
     income: Double;
     expend: Double;
     bz: String;
     del: Integer;
     status: Integer;
     create_time: TDATETIME;
     plan_time:   TDATETIME;
     constructor Create; overload;
     constructor Create(fdquery: TFDQuery);overload;
     procedure loadFromData(fdquery: TFDQuery);
end;


//     id          INTEGER  PRIMARY KEY AUTOINCREMENT,
//    name        TEXT     NOT NULL,
//    classify    INT      NOT NULL,
//    money       REAL     NOT NULL,
//    bz          TEXT     NOT NULL,
//    del         INT      NOT NULL,
//    create_time DATETIME,
//    plan_time   DATETIME,
//    msid        INTEGER  DEFAULT (0)
type
  TPlan = class
  public
    id: Integer;
    classify: Integer;
    outin: Integer;
    bz: String;
    money: Double;
    del: Integer;
    create_time: TDateTime;
    plan_time: TDateTime;
    msid: Integer;
    constructor Create; overload;
    constructor Create(fdquery: TFDQuery);overload;
    procedure loadFromData(fdquery: TFDQuery);
end;

type
  TDictionary = class
  public
  id: Integer;
  code_cate: String;
  code: Integer;
  code_name: String;
  remark: String;
  constructor Create; overload;
  constructor Create(fdquery: TFDQuery);overload;
  procedure loadFromData(fdquery: TFDQuery);
end;

implementation

{ TMonthSettings }

constructor TMonthsSetting.Create;
begin
   //
   inherited;

end;

constructor TMonthsSetting.Create(fdquery: TFDQuery);
begin

  //
   Create;
   loadFromData(fdquery);
end;

procedure TMonthsSetting.loadFromData(fdquery: TFDQuery);
begin
  //
  id := fdquery.FieldByName('id').AsInteger;
  year := fdquery.FieldByName('year').AsInteger;
  month := fdquery.FieldByName('month').AsInteger;
  budget := fdquery.FieldByName('budget').AsFloat;
  income := fdquery.FieldByName('income').AsFloat;
  expend := fdquery.FieldByName('expend').AsFloat;
  bz := fdquery.FieldByName('bz').AsString;
  del := fdquery.FieldByName('del').AsInteger;
  status := fdquery.FieldByName('status').AsInteger;
  create_time := fdquery.FieldByName('create_time').AsDateTime;
  plan_time := fdquery.FieldByName('plan_time').AsDateTime;

end;

{ TPlan }

constructor TPlan.Create;
begin
 inherited;
end;

constructor TPlan.Create(fdquery: TFDQuery);
begin
   Create;
   loadFromData(fdquery);
end;

procedure TPlan.loadFromData(fdquery: TFDQuery);

begin
    id := fdquery.FieldByName('id').AsInteger;
    classify := fdquery.FieldByName('month').AsInteger;
    outin := fdquery.FieldByName('budget').AsInteger;
    money := fdquery.FieldByName('income').AsFloat;
    bz := fdquery.FieldByName('bz').AsString;
    del := fdquery.FieldByName('del').AsInteger;
    msid := fdquery.FieldByName('status').AsInteger;
    create_time := fdquery.FieldByName('create_time').AsDateTime;
    plan_time := fdquery.FieldByName('plan_time').AsDateTime;
end;

{ TDictionary }

constructor TDictionary.Create;
begin
  inherited;
end;

constructor TDictionary.Create(fdquery: TFDQuery);
begin
  Create;
   loadFromData(fdquery);
end;

procedure TDictionary.loadFromData(fdquery: TFDQuery);
begin
  Self.id := fdquery.FieldByName('id').AsInteger;
  Self.code_cate := fdquery.FieldByName('code_cate').AsString;
  Self.code := fdquery.FieldByName('code').AsInteger;
  Self.code_name := fdquery.FieldByName('code_name').AsString;
  Self.remark := fdquery.FieldByName('code_name').AsString;
end;

end.
