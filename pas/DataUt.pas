unit DataUt;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.FMXUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
   FireDAC.Comp.Client,Entity,Generics.Collections, FireDAC.Comp.DataSet,
   FireDAC.Comp.UI;


const classify: array[0..7] of String =('早餐','午餐','晚餐','零食','生活用品','买菜','电费','其他');
      outin: array[0..1] of String =('支出','收入');
type
  // TSqlType = [];
  TDataModule1 = class(TDataModule)
    BaseConnection: TFDConnection;
    fdquery1: TFDQuery;
    tableInit: TFDQuery;
    planfdquery: TFDQuery;
    msfdquery: TFDQuery;
    dictionaryfdquery: TFDQuery;
    planfdqueryid: TFDAutoIncField;
    planfdqueryclassify: TIntegerField;
    planfdqueryoutin: TIntegerField;
    planfdquerymoney: TFloatField;
    planfdquerybz: TWideMemoField;
    planfdquerydel: TIntegerField;
    planfdquerycreate_time: TDateTimeField;
    planfdqueryplan_time: TDateTimeField;
    planfdquerymsid: TIntegerField;
    dictionaryfdqueryid: TFDAutoIncField;
    dictionaryfdquerycode_cate: TWideMemoField;
    dictionaryfdquerycode: TIntegerField;
    dictionaryfdquerycode_name: TWideMemoField;
    dictionaryfdqueryremark: TWideMemoField;
    msfdqueryid: TFDAutoIncField;
    msfdqueryyear: TIntegerField;
    msfdquerymonth: TIntegerField;
    msfdquerybudget: TFloatField;
    msfdqueryincome: TFloatField;
    msfdqueryexpend: TFloatField;
    msfdquerybz: TWideMemoField;
    msfdquerydel: TIntegerField;
    msfdquerystatus: TIntegerField;
    msfdquerycreate_time: TDateTimeField;
    msfdqueryplan_time: TDateTimeField;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    procedure DataModuleCreate(Sender: TObject);
    procedure planfdqueryoutinGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure planfdqueryoutinSetText(Sender: TField; const Text: string);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    procedure MonthsSettingInit;//预算表每一个月起始需要新增一条数据
    procedure PlanToMonthData;  //把Plan中的月收入，支出的数据同步到MonthsSetting中
    function UpdateMonth(id:Integer;outin:integer;money: Double): Boolean;      //更新一条数据
    procedure DictionaryInit;

    function dicloadData: Boolean;
  public
    { Public declarations }
    function execSql(sqlStr: String):Boolean;
     procedure writeLog(text: String);
  end;

var
  DataModule1: TDataModule1;
  dicList: TList<TDictionary>;
implementation
  uses System.IOUtils;
{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TDataModule1.DataModuleCreate(Sender: TObject);
begin
    dicList := TList<TDictionary>.Create;
     Self.writeLog('程序启动');
    //
    {$IFDEF ANDROID}
       BaseConnection.Params.Values['DataBase'] := TPath.Combine(TPath.GetDownloadsPath,'cenPlan.sdb');
    {$ENDIF}

    {$IFDEF MSWINDOWS}
      //.\cenPlan.sdb
      BaseConnection.Params.Values['DataBase'] := TPath.Combine('.\','cenPlan.sdb');
    {$ENDIF}
    try

      BaseConnection.Connected := True;
      tableInit.ExecSQL;
      dictionaryInit;
      MonthsSettingInit;
     // dictionaryfdquery.fie
    except on e : Exception do
      //ShowMessage('数据库连接失败');
     //写入日志 和异常处理
     Self.writeLog('程序连接数据库失败');
    end;

end;

procedure TDataModule1.DataModuleDestroy(Sender: TObject);
begin
   FreeAndNil(dicList);
end;

function TDataModule1.dicloadData: Boolean;
var sqlStr: String;
  I,count: Integer;
begin
  //
  with dictionaryfdquery do
  begin
      Open;
      if RecordCount>0 then Exit;
      sqlStr := 'insert into Dictionary(code_cate,code,code_name,remark) Values(:p1,:p2,:p3,:p4)';
      SQL.Clear;
      SQL.Text := sqlStr;

      //{}
      Params.ArraySize := Length(outin) + Length(classify);
      count := 0;
      for I := 0 to Length(outin)-1 do
      begin
        Params[0].Values[count] := 'outin';
        Params[1].Values[count] := I+1;
        Params[2].Values[count] := outin[I];
        Params[3].Values[count] := '1为支出2为收入';
        count := count +1;
      end;

      for I := 0 to Length(classify)-1 do
      begin
        Params[0].Values[count] := 'classify';
        Params[1].Values[count] := I+1;
        Params[2].Values[count] := classify[I];
        Params[3].Values[count] := '分类';
        count := count +1;
      end;
      Execute(Params.ArraySize,0);       //带参数指定执行数量
      RowsAffected; // 显示多少条执行成功
//    Edit;          这么写只能修改或增加一条数据
//    FieldValues['code_cate'] := 'a';
//    FieldValues['code'] := 2;
//    FieldValues['code_name'] := 'a';
//    FieldValues['remark'] := 'a';
//    Post;
  end;
  Result := True;
end;

procedure TDataModule1.DictionaryInit;
var dic: TDictionary; sqlStr: String;
begin
  //
  dicloadData;
  with dictionaryfdquery do
  begin
    sqlStr := 'select * from Dictionary';
    SQL.Clear;
    SQL.Text := sqlStr;
    Open;
    if RecordCount <= 0 then  Exit;
    First;
    while not Eof do
    begin
      dic := TDictionary.Create;
      dic.id := FieldValues['id'];
      dic.code_cate := FieldValues['code_cate'];
      dic.code      := FieldValues['code'];
      dic.code_name := FieldValues['code_name'];
      dic.remark    := FieldValues['remark'];
      dicList.Add(dic);
      Next;
    end;
  end;

end;

function TDataModule1.execSql(sqlStr: String): Boolean;
begin
   //
   with fdquery1 do
   begin
      Close;
      SQL.Clear;
      SQL.Text := sqlStr;
      Open;
      //if RecordCount>0 then

   end;

   Result := True;
end;

procedure TDataModule1.MonthsSettingInit;
var sqlStr: String;
    toMonth,toYear,toTime:string;
begin
  toMonth := FormatDateTime('mm',now);
   toYear := FormatDateTime('yyyy',now);
   toTime := FormatDateTime('yyyy-mm-dd HH:mm:ss',now);
   sqlStr := 'select * from MonthsSetting where del=1 and year=:P1 and month=:P2' ;
   with msfdquery do
   begin
     SQL.Clear;
     SQL.Text := sqlStr;
     Params[0].Values[0] := toYear;
     Params[1].Values[0] := toMonth;
     Open;
     if RecordCount<=0 then   //只有当query的返回结果不存在，才能新增数据。不然就是修改
     begin
        Edit;
        // FieldValues['year'] := toYear;
        msfdqueryyear.Value := toYear.ToInteger;
        msfdquerymonth.Value := toMonth.ToInteger;
        msfdquerybudget.Value := 500.00;
        msfdqueryincome.Value := 0.00;
        msfdqueryexpend.Value := 0.00;
        msfdquerybz.Value := toTime;
        msfdquerydel.Value := 1;
        msfdquerystatus.Value := 1;
        msfdquerycreate_time.Value := now;
        msfdqueryplan_time.Value := now;
        Post;
     end;
   end;
end;

procedure TDataModule1.planfdqueryoutinGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
   //
  //、、 Text := 'aaa';
end;

procedure TDataModule1.planfdqueryoutinSetText(Sender: TField;
  const Text: string);
begin
  //Sender.is
end;

procedure TDataModule1.PlanToMonthData;
var sqlStr,sqlStr_1: String;
  I: Integer;
begin
//select sum(p.money) as money,m.month,m.year,p.outin,m.income,m.budget  from Plan p
//Left join MonthsSetting m
//on p.msid = m.id
//where p.del = 1 And m.del = 1
//group by p.msid,p.outin
   sqlStr := 'select sum(p.money) as money,m.month,m.year,p.outin,m.income,m.budget,m.expend,m.id  from Plan p '+
   'Left join MonthsSetting m on p.msid = m.id where p.del = 1 And m.del = 1 group by p.msid,p.outin ';
   with fdquery1 do
   begin
     SQL.Clear;
     SQL.Text := sqlStr;
     Open;
     if RecordCount<=0 then Exit;
     First;
     while not Eof do
     begin
       if FieldByName('outin').AsInteger = 1 then    //1支出
       begin
           if FieldByName('money').AsFloat <> FieldByname('expend').AsFloat then
           begin
             //更新
           end;
       end;
       if FieldByName('outin').AsInteger = 2 then    //2收入
       begin
           if FieldByName('money').AsFloat <> FieldByname('income').AsFloat then
           begin
             //更新
           end;
       end;

     end;
   end;
end;

function TDataModule1.UpdateMonth(id, outin: integer; money: Double): Boolean;
var sqlStr: String;
begin
   //
   sqlStr := 'select * from MonthsSetting where id = :p1';
   with msfdquery do
   begin
     SQL.Clear;
     SQL.Text := sqlStr;
     Params[0].Values[0] := id;
     Open;
     if RecordCount<=0 then
     begin
        Result := False;
     end
     else
     begin
       Edit;
     //   msfdqueryyear.Value := toYear.ToInteger;
     //   msfdquerymonth.Value := toMonth.ToInteger;
     //   msfdquerybudget.Value := 500.00;
        msfdqueryincome.Value := 0.00;
        msfdqueryexpend.Value := 0.00;
       // msfdquerybz.Value := toTime;
      //  msfdquerydel.Value := 1;
      //  msfdquerystatus.Value := 1;
      //  msfdquerycreate_time.Value := now;
      //  msfdqueryplan_time.Value := now;
      post;
     end;
   end;
end;

procedure TDataModule1.writeLog(text: String);
var s,mfile: string;
   filev: TextFile;
begin
  //
    s := FormatDateTime('yyyy-mm-dd HH:mm:ss',now)+ ':     ';
   {$IFDEF MSWINDOWS}
     // s :=  TPath.Combine('.\log','mylog.txt');

     if not DirectoryExists(ExtractFilePath(ParamStr(0))+'log') then
     MkDir(ExtractFilePath(ParamStr(0))+'log');

     mfile := ExtractFilePath(ParamStr(0))+'log\' + FormatDateTime('yyyy-mm-dd', now) + '.txt';
     if FileExists(mfile) then
     begin
        AssignFile(filev,mfile);
        append(filev);
        writeln(filev,s+text);

     end
     else
     begin
        AssignFile(filev,mfile);
        ReWrite(filev);
        writeln(filev,s+text);
     end;
     CloseFile(filev);
   {$ENDIF}

      {$IFDEF ANDROID}

      if not TDirectory.Exists(TPath.GetPublicPath + '/logs') then
      TDirectory.CreateDirectory(TPath.GetPublicPath + '/logs');


//     if not DirectoryExists(TPath.GetDownloadsPath+'log') then
//     MkDir(TPath.GetDownloadsPath+'log');
      mfile := TPath.Combine(TPath.GetPublicPath + '/logs',FormatDateTime('yyyy-mm-dd', now) + '.txt') ;
       if FileExists(mfile) then
     begin
        AssignFile(filev,mfile);
        append(filev);
        writeln(filev,s+text);

     end
     else
     begin
        AssignFile(filev,mfile);
        ReWrite(filev);
        writeln(filev,s+text);
     end;
     CloseFile(filev);
   {$ENDIF}
end;

end.
