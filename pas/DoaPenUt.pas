unit DoaPenUt;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.ScrollBox, FMX.Memo, FMX.DateTimeCtrls, FMX.Edit, FMX.ComboEdit,
  FMX.Layouts, FMX.StdCtrls, FMX.Controls.Presentation,DataUt, FMX.EditBox,Entity,
  FMX.NumberBox,serviceUt;

type
  TaPenForm = class(TForm)
    ToolBar1: TToolBar;
    SpeedButton2: TSpeedButton;
    SpeedButton1: TSpeedButton;
    Text1: TText;
    mainLayout: TLayout;
    Layout1: TLayout;
    Layout2: TLayout;
    Text2: TText;
    Layout3: TLayout;
    Text3: TText;
    ComboEdit1: TComboEdit;
    Layout4: TLayout;
    Text4: TText;
    Layout5: TLayout;
    Text5: TText;
    DateEdit1: TDateEdit;
    Layout6: TLayout;
    Text6: TText;
    bz: TMemo;
    Layout7: TLayout;
    Rectangle2: TRectangle;
    Text7: TText;
    Rectangle1: TRectangle;
    Text10: TText;
    TimeEdit1: TTimeEdit;
    money: TNumberBox;
    out: TRadioButton;
    income: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure Text7Click(Sender: TObject);
    procedure Text10Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  aPenForm: TaPenForm;

implementation

{$R *.fmx}

procedure TaPenForm.FormCreate(Sender: TObject);
var
  I: Integer;
begin
   //1页面初始化 时间使用最新时间
   //2 绑定combobox数据
   //DateEdit1.TodayDefault := True;

   TimeEdit1.UseNowTime := True;
   for I := 0 to DataUt.dicList.Count -1  do
   begin
    // ComboEdit1.Items.Add(DataUt.dicList[I].code_name);
    if dicList[I].code_cate = 'classify' then
    begin
    ComboEdit1.Items.Add(DataUt.dicList[I].code_name);
      //ComboEdit1.Items.AddObject(DataUt.dicList[I].code_name,TObject(DataUt.dicList[I].code.ToString));
    end;

      // ComboEdit1.Items.Objects[ComboEdit1.ItemIndex];
   end;

   ComboEdit1.ItemIndex := 0;
end;

procedure TaPenForm.FormDestroy(Sender: TObject);
begin
   TimeEdit1.UseNowTime := False;
end;

procedure TaPenForm.FormShow(Sender: TObject);
var
  I: Integer;
begin
  //1页面初始化 时间使用最新时间
  DateEdit1.Date := now  ;
   //2 绑定combobox数据
   //DateEdit1.TodayDefault := True;
//   TimeEdit1.UseNowTime := True;
//   for I := 0 to DataUt.dicList.Count -1  do
//   begin
//    // ComboEdit1.Items.Add(DataUt.dicList[I].code_name);
//    if dicList[I].code_cate = 'classify' then
//    begin
//      ComboEdit1.Items.AddObject(DataUt.dicList[I].code_name,TObject(DataUt.dicList[I].code.ToString));
//    end;
//
//      // ComboEdit1.Items.Objects[ComboEdit1.ItemIndex];
//   end;
//
//   ComboEdit1.ItemIndex := 0;
end;

procedure TaPenForm.SpeedButton1Click(Sender: TObject);
begin
  Self.Close;
end;

procedure TaPenForm.Text10Click(Sender: TObject);
begin

 Self.Close;
end;

procedure TaPenForm.Text7Click(Sender: TObject);
var pData: TPlan;
   dateStr,timeStr: string;
begin
  //
  pData := TPlan.Create;
  pData.classify := ComboEdit1.ItemIndex + 1;

  if out.IsChecked then
  begin
    pData.outin := 1;
  end
  else
  begin
    pData.outin := 2;
  end;

  pData.bz := bz.Text;
  pData.money := money.Text.ToDouble;
  pData.del := 1;
  pData.create_time := now;

  dateStr := FormatDateTime('yyyy-mm-dd',DateEdit1.DateTime);
  timeStr := FormatDateTime('HH:mm:ss',TimeEdit1.DateTime);
 // pData.plan_time := StrToDateTime(dateStr+' '+timeStr);
  pData.plan_time := DateEdit1.DateTime + TimeEdit1.DateTime;
  pData.msid := getmsid(now);

  if addPlan(pData) then
  Self.Close;

end;

end.
