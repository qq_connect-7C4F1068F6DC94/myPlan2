unit mainFrameUt;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView, FMX.Objects, FMX.Controls.Presentation, FMX.Layouts,DoaPenUt,
  System.ImageList, FMX.ImgList;

type
  TmainFrame = class(TFrame)
    mainLayout: TLayout;
    Rectangle1: TRectangle;
    dayexpendLayout: TLayout;
    dayexpendLab: TLabel;
    dayexpendText: TText;
    monthexpendLayout: TLayout;
    monthexpendLab: TLabel;
    monthexpendText: TText;
    monthbudgetLayout: TLayout;
    monthbudgetLab: TLabel;
    monthbudgetText: TText;
    monthhaveLayout: TLayout;
    monthhaveLab: TLabel;
    monthhaveText: TText;
    Layout4: TLayout;
    monthprolabel: TLabel;
    monthprobar: TProgressBar;
    monthincomeLayout: TLayout;
    monthincomeLab: TLabel;
    monthincomeText: TText;
    Circle1: TCircle;
    Text1: TText;
    Rectangle2: TRectangle;
    ListView1: TListView;
    ImageList2: TImageList;
    todayls: TText;
    totalls: TText;
    expendls: TText;
    Layout1: TLayout;
    incomels: TText;
    procedure Text1Click(Sender: TObject);
  private
    { Private declarations }
    procedure setpro(val: Double; maxval: Double);
    procedure setlistview;
  public
    { Public declarations }
    procedure framerefresh;

  end;

implementation
 uses ServiceUt,StrToolUt,Entity,Generics.Collections;
{$R *.fmx}

procedure TmainFrame.framerefresh;
 var expend,income,budget: double;
begin
  dayexpendText.Text :=  IntToFloatStr(ServiceUt.getToDay(False).ToString,0);
  expend  := 0;
  income  := 0;
  if getToMonth(expend,income) then
  begin
    monthexpendText.Text := IntToFloatStr(expend.ToString,0);
    monthincomeText.Text := IntToFloatStr(income.ToString,0);
  end
  else
  begin
     monthexpendText.Text := '0.00';
     monthincomeText.Text := '0.00';
  end;

  budget := getToMonthBudget(False);
  monthbudgetText.Text := IntToFloatStr(budget.ToString,0);
  monthhaveText.Text := IntToFloatStr((getToMonthBudget(False) - expend).ToString,0);

  setpro(expend,budget);
  setlistview;
end;

procedure TmainFrame.setlistview;
var startD,endD: String;
    plans : TList<TPlan>;
    I,incomeCount: Integer;
    AItem : TListViewItem;
begin
  //
  ListView1.Items.Clear;
  incomeCount := 0;
  Randomize;
  startD := FormatDateTime('yyyy-mm-dd',now)+' '+'00:00:00';
  endD   := FormatDateTime('yyyy-mm-dd',now)+' '+'23:59:59';
  plans := getPlanByDate(startD,endD);
  if plans.Count > 0  then
  begin
    for I := 0 to plans.Count -1  do
    begin
       AItem := ListView1.Items.Add;
       AItem.Data['Text1'] := getDic(plans[I].classify,'classify');
       AItem.Data['Text3'] := plans[I].bz;
       AItem.Data['Text4'] := FormatDateTime('HH:mm',plans[I].plan_time);
       AItem.Data['Text5'] := setmoney(plans[I].money,plans[I].outin);
       AItem.Data['Image2'] := Random(4);

       if plans[I].outin = 2 then
       begin
         incomeCount := incomeCount + 1;
       end;

    end;
  end;

  //面板数据
  todayls.Text := FormatDateTime('yyyy年mm月dd日',now) ;
  totalls.Text := '一共' + plans.Count.ToString + '笔';
  expendls.Text := '支出:' + (plans.Count - incomeCount).ToString + '笔';
  incomels.Text := '收入:' + (incomeCount).ToString + '笔';
end;

procedure TmainFrame.setpro(val: Double; maxval: Double);
begin
  //
  monthprobar.Max := maxval;
  monthprobar.Value := val;
end;

procedure TmainFrame.Text1Click(Sender: TObject);
//var asPenForm: TaPenForm;
begin
//
    if not Assigned(aPenForm) then
    begin
    aPenForm := TaPenForm.Create(nil);
     end;

  {$IFDEF MSWINDOWS}
  //aPenForm := TaPenForm.Create(nil);
  if aPenForm.ShowModal = mrNone then
  begin
     //

  end;
  if aPenForm <> nil then
  begin
  FreeAndNil(aPenForm);
  end;          //todo 无法释放
  framerefresh;
  {$ENDIF MSWINDOWS}

  {$IFDEF ANDROID}   // {$IFDEF Android}
//  if not Assigned(aPenForm) then
//  begin
//    aPenForm := TaPenForm.Create(nil);
//  end;
      aPenForm.ShowModal(procedure(modalResult: TModalResult)
      begin
         if modalResult = mrNone then
         begin
            //
            framerefresh;
         end;
      end
      );
  //aPenForm.Show;
  if aPenForm <> nil then
  begin
  FreeAndNil(aPenForm);
  end;          //todo 无法释放
  framerefresh;
  {$ENDIF}
end;

end.
