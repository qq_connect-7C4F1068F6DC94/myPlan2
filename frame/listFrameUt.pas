unit listFrameUt;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Layouts, FMX.ListBox, FMX.DateTimeCtrls;

type
  TlistFrame = class(TFrame)
    background: TRectangle;
    mainLayout: TLayout;
    tabgle: TRectangle;
    tabLayout: TLayout;
    yeartabgle: TRectangle;
    monthtabgle: TRectangle;
    daytabgle: TRectangle;
    yeartabtext: TText;
    daytabtext: TText;
    monthtabtext: TText;
    detailLayout: TLayout;
    mainListBox: TListBox;
    procedure yeartabtextClick(Sender: TObject);
  private
    { Private declarations }

    procedure getlist(year: Integer; month: Integer);
  public
    { Public declarations }
    procedure listrefresh;


  end;

implementation
 uses serviceUt,Entity,Generics.Collections;
{$R *.fmx}

{ TlistFrame }

procedure TlistFrame.getlist(year: Integer; month: Integer);
var AItem: TListBoxItem;
    lRTDataByTypes: TList<RTDataByTypes>;
    adata : RTDataByTypes;
    rPlan : TPlan;
    rPlans : TList<TPlan>;
    itemDatas : Tlist<RTListAItemData>;
    itemData :RTListAItemData;
    test,tsd,stype: String;
    I: Integer;
begin
  //


     itemDatas := getDataByTypes(year,month);
     //ShowMessage(itemDatas.Count.ToString);
      mainListBox.Items.Clear;
     if itemDatas.Count <= 0  then    Exit;


     //mainListBox.
     tsd := itemDatas[0].data_0;
     case itemDatas[0].data_id of
        1 : begin
          stype := '年'
        end;
        2 : begin
           stype := '月';
        end;
        3 : begin
           stype := '号';
        end;
     end;
     for I := 0 to itemDatas.Count - 1  do
     begin
        if (tsd = itemDatas[I].data_0) and (I <> 0) then
        begin
           AItem := mainListBox.ItemByIndex(I-1);
           if itemDatas[I].data_outin = 2 then
           begin
              AItem.StylesData['ts1'] := '收入:'+itemDatas[I].data_num.ToString + ' 笔';
              AItem.StylesData['ts2'] := '共 '+itemDatas[I].data_money.ToString + ' 元';
           end;

           if itemDatas[I].data_outin = 1 then
           begin
               AItem.StylesData['ts3'] := '支出:'+itemDatas[I].data_num.ToString + ' 笔';
               AItem.StylesData['ts4'] := '共 '+itemDatas[I].data_money.ToString + ' 元';
           end;

        end
        else
        begin
           AItem :=  TListBoxItem.Create(nil);
           AItem.StyleLookup := 'cenListBoxStyle2';
           AItem.Parent := mainListBox;
           AItem.StylesData['ts0'] := itemDatas[I].data_0 + stype;

           if itemDatas[I].data_outin = 2 then
           begin
              AItem.StylesData['ts1'] := '收入:'+itemDatas[I].data_num.ToString + ' 笔';
              AItem.StylesData['ts2'] := '共 '+itemDatas[I].data_money.ToString + ' 元';
              AItem.StylesData['ts3'] := '支出:'+0.ToString + ' 笔';
              AItem.StylesData['ts4'] := '共 '+0.ToString + ' 元';
           end;

           if itemDatas[I].data_outin = 1 then
           begin
               AItem.StylesData['ts1'] := '收入:'+0.ToString + ' 笔';
               AItem.StylesData['ts2'] := '共 '+0.ToString + ' 元';
               AItem.StylesData['ts3'] := '支出:'+itemDatas[I].data_num.ToString + ' 笔';
               AItem.StylesData['ts4'] := '共 '+itemDatas[I].data_money.ToString + ' 元';
           end;
           //AItem.StylesData['ts1'] := itemDatas[I].data_0;
           tsd :=  itemDatas[I].data_0;
        end;
     end;

    itemDatas.Free;
end;

procedure TlistFrame.listrefresh;

begin
  //

   yeartabtextClick(yeartabtext);
end;

procedure TlistFrame.yeartabtextClick(Sender: TObject);
var year,month: Integer;
begin
   //

   year := FormatDateTime('yyyy',now).ToInteger;
   month := FormatDateTime('mm',now).ToInteger;

   case TText(Sender).Tag of
   1 : begin
      monthtabgle.Fill.Color := TAlphaColorRec.Red;
      daytabgle.Fill.Color :=  TAlphaColorRec.Red;
      yeartabgle.Fill.Color :=  TAlphaColorRec.Yellow;
      getList(0,0);

   end;

   2 : begin
      monthtabgle.Fill.Color := TAlphaColorRec.Yellow;
      daytabgle.Fill.Color :=  TAlphaColorRec.Red;
      yeartabgle.Fill.Color :=  TAlphaColorRec.Red;
      getList(year,0);
   end;

   3 : begin
       monthtabgle.Fill.Color := TAlphaColorRec.Red;
       daytabgle.Fill.Color :=  TAlphaColorRec.Yellow;
      yeartabgle.Fill.Color :=  TAlphaColorRec.Red;

       getList(year,month);
   end;
   end;
end;

end.
