unit deviceUt;
{
   设备工具类      20200403  刘成涛

}
interface
uses FMX.PlatForm,
      {$IFDEF ANDROID}
      Androidapi.Helpers,
      Androidapi.JNI.GraphicsContentViewText,
      {$ENDIF}
      System.Types;

function getScreen(checked: boolean):boolean;
function setBarColor(checked: boolean;color: Integer):boolean;
//获取本地版本号
function getLocalVersion: String;
{Android application 事件 }
procedure SetApplicationEvent;
var  screenwidth: Single;
     screenHeight: Single;
implementation

function getLocalVersion: String;
var
   AppSvc: IFMXApplicationService;
begin
  if TPlatformServices.Current.SupportsPlatformService(IFMXApplicationService,
    IInterface(AppSvc)) then
  begin
    Result := AppSvc.AppVersion;
  end
  else
  begin
    Result := '';
  end;
end;
procedure SetApplicationEvent;
begin
   //
end;
function setBarColor(checked: boolean;color: Integer):boolean;
begin
  {$IFDEF ANDROID}
    TAndroidHelper.Activity.getWindow.setStatusBarColor(color);
  {$ENDIF}
end;
function getScreen(checked: boolean): boolean;
var
  ScreenSvc: IFMXScreenService;
  Size: TPointF;
  iw: Integer;
begin
  if TPlatformServices.Current.SupportsPlatformService  (IFMXScreenService, IInterface(ScreenSvc))  then
  begin
    Size := ScreenSvc.GetScreenSize;
    screenwidth := Size.X;
    screenHeight := Size.Y;
    iw := Trunc(screenwidth) mod 4;
    if iw = 0 then
    begin
      Result := True;
    end
    else
    begin
      Result := False;
    end;
  end
  else
  begin
     screenwidth := 0;
    screenHeight := 0;
    Result := False;
  end;
end;
end.
