unit StrToolUt;
     {字符串工具类  20200407 刘成涛}
interface
uses System.SysUtils;
//给字符串后面加0. 500 ————> 500.00
{输入参数 intstr 目标数据 sizei 几个0}
function IntToFloatStr(intstr: string;sizei: Integer): String;

//从数据库获取的money 处理 5 ———>  -￥5.00
function setmoney(money: Double; outin: Integer): String;
implementation

function setmoney(money: Double; outin: Integer): String;
var outinStr : String;
begin
   if outin = 2 then
   begin
     outinStr := '+';
   end
   else
   begin
     outinStr := '-';
   end;

   Result := outinStr + '￥' + money.ToString +'.00';
end;

function IntToFloatStr(intstr: string;sizei: Integer): String;
var checked: Boolean;
    resultStr: String;
begin
  if SameText('',intstr) then
  begin
    resultStr := '0';
  end
  else
  begin
    resultStr := intstr;
  end;

  if sizei = 0 then
    resultStr := resultStr +'.00';
  Result := resultStr;
end;
end.
