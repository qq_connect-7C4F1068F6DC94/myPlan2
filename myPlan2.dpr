program myPlan2;

uses
  System.StartUpCopy,
  FMX.Forms,
  mainUt in 'mainUt.pas' {mainFrm},
  DataUt in 'pas\DataUt.pas' {DataModule1: TDataModule},
  Entity in 'pas\Entity.pas',
  startFrm in 'pas\startFrm.pas' {Form2},
  serviceUt in 'pas\serviceUt.pas',
  ownerFrameUt in 'frame\ownerFrameUt.pas' {ownerFrame},
  deviceUt in 'tool\deviceUt.pas',
  mainFrameUt in 'frame\mainFrameUt.pas' {mainFrame: TFrame},
  listFrameUt in 'frame\listFrameUt.pas' {listFrame: TFrame},
  countFrameUt in 'frame\countFrameUt.pas' {Frame3: TFrame},
  DoaPenUt in 'pas\DoaPenUt.pas' {aPenForm},
  StrToolUt in 'tool\StrToolUt.pas';

{$R *.res}

begin
  Application.Initialize;

  Application.CreateForm(TDataModule1, DataModule1);
  Application.CreateForm(TmainFrm, mainFrm);
  // Application.CreateForm(TForm2, Form2);
//  Application.CreateForm(TaPenForm, aPenForm);
  Application.Run;

end.
