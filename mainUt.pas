unit mainUt;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects, FMX.Layouts,
  System.ImageList, FMX.ImgList,mainFrameUt,listFrameUt,ownerFrameUt,FMX.PlatForm,deviceUt;

type
  TmainFrm = class(TForm)
    mainLayout: TVertScrollBox;
    ImageList2: TImageList;
    ImageList1: TImageList;
    frameLayout: TLayout;
    Rectangle1: TRectangle;
    r1: TRectangle;
    t1: TText;
    Rectangle3: TRectangle;
    r3: TRectangle;
    t3: TText;
    Rectangle5: TRectangle;
    r4: TRectangle;
    t4: TText;
    Rectangle7: TRectangle;
    r2: TRectangle;
    t2: TText;
    GridLayout1: TGridLayout;
    Layout1: TLayout;
    Layout2: TLayout;
    Layout3: TLayout;
    Layout4: TLayout;
    i1: TImage;
    Text1: TText;
    Text2: TText;
    Text3: TText;
    Text4: TText;
    i2: TImage;
    i3: TImage;
    i4: TImage;
    StyleBook1: TStyleBook;
    procedure r1Click(Sender: TObject);
    procedure frameBarLayoutClick(Sender: TObject);
    procedure frameShow(iframe: Byte);
    procedure appevent;
    function HandleAppEvent(AAppEvent: TApplicationEvent; AContext: TObject)   : boolean;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  mainFrm: TmainFrm;
  mainFrame: TMainFrame;
  listFrame: TListFrame;
  ownerFrame : TOwnerFrame;
implementation
   uses System.IOUtils;
{$R *.fmx}

procedure TmainFrm.appevent;
begin
   {$IF ANDROID}
   // 后台事件
   if TPlatformServices.Current.SupportsPlatformService(IFMXApplicationEventService, IInterface(IFMXApplicationEventService)) then
    IFMXApplicationEventService.SetApplicationEventHandler(HandleAppEvent);
   {$ENDIF}


end;
function TmainFrm.HandleAppEvent(AAppEvent: TApplicationEvent; AContext: TObject): Boolean;
begin
     // APP进入到后台，10秒之内切回到前台，不做二次验证。
  // APP进入到后台，超过10秒切回到前台，再次进行指纹验证。
  // 记录进入后台的时间data 时入后台时检查时间差 10以内不需要验证
  case AAppEvent of
    TApplicationEvent.FinishedLaunching:
     begin
        ShowMessage('FinishedLaunching');
     end;
    TApplicationEvent.BecameActive: // 主窗体重新激活也会触发 很频繁一般建议少用
     begin
       ShowMessage('BecameActive'); // 第一次运行app触发,从后台切换过来也触发
     end;
    TApplicationEvent.WillBecomeInactive:
     begin
    ShowMessage('WillBecomeInactive')
     end;
    TApplicationEvent.EnteredBackground:
    begin
       ShowMessage('EnteredBackground')  //切换到后台
    end;
    TApplicationEvent.WillBecomeForeground:
    begin
       ShowMessage('WillBecomeForeground'); // 从后台切换到前台
//      if Gsettingjson.TimeLimit <> tlNone then
//      begin
//
//
//      end;
    end;
    TApplicationEvent.WillTerminate:
    begin
       ShowMessage('WillTerminate');
    end;
    TApplicationEvent.LowMemory:
    begin
       ShowMessage('LowMemory');
    end;

    TApplicationEvent.TimeChange:
    begin
      ShowMessage('TimeChange');
    end;

    TApplicationEvent.OpenURL:
    begin
      ShowMessage('OpenURL');
    end;

  end;
    Result := True;
end;


procedure TmainFrm.FormCreate(Sender: TObject);
begin
 //   appevent;

 deviceUt.setBarColor(false,TAlphaColorRec.Red);
 frameBarLayoutClick(i1);
 //Rectangle1.Fill.Co
end;

procedure TmainFrm.frameBarLayoutClick(Sender: TObject);
var
    sizef: TSizeF;
    textclick: TText;
  I: Integer;
begin
   //

   sizef.cx := 32;
   sizef.cy := 32;
   if TImage(Sender).Tag <= 0 then Exit;

   for I := 1 to 4 do
   begin
     if I = TImage(Sender).Tag then
     begin
        TImage(Sender).Bitmap := ImageList1.Bitmap(sizef,I-1);
        (findComponent('Text'+I.ToString) As TText).TextSettings.FontColor := TAlphaColorRec.Black;
     end
     else
     begin
        (findComponent('i'+I.ToString) As TImage).Bitmap := ImageList2.Bitmap(sizef,I-1);
        (findComponent('Text'+I.ToString) As TText).TextSettings.FontColor := TAlphaColorRec.Gray;
     end;


   end;
   frameshow(TImage(Sender).Tag);
end;

procedure TmainFrm.frameShow(iframe: Byte);
begin
  //
  case iframe of
    1 : begin
       if listFrame <> nil then
          listFrame.Parent := nil;
       if ownerFrame <> nil then
          ownerFrame.Parent := nil;


       if mainFrame = nil then
          mainFrame := TMainFrame.Create(mainLayout);
          mainFrame.framerefresh;
       mainFrame.Parent := mainLayout;
       mainFrame.Width := mainLayout.Width;
       mainFrame.Height := mainLayout.Height;
    end;

    2 : begin
       if mainFrame <> nil then
          mainFrame.Parent := nil;
       if ownerFrame <> nil then
          ownerFrame.Parent := nil;


       if listFrame = nil then
          listFrame := TListFrame.Create(mainLayout);
          listFrame.listrefresh;
       listFrame.Parent := mainLayout;
       listFrame.Width := mainLayout.Width;
       listFrame.Height := mainLayout.Height;
    end;

    3 : begin

    end;

    4 : begin
        if mainFrame <> nil then
          mainFrame.Parent := nil;
       if listFrame <> nil then
          listFrame.Parent := nil;


       if ownerFrame = nil then
          ownerFrame := TOwnerFrame.Create(mainLayout);
       ownerFrame.Parent := mainLayout;
       ownerFrame.Width := mainLayout.Width;
       ownerFrame.Height := mainLayout.Height;
    end;
  end;
end;



procedure TmainFrm.r1Click(Sender: TObject);
var I: Integer;
 a: TSizeF;
 ft : TText;
begin
  //
     a.cx := 32;
     a.cy := 32;
     for I := 1 to 4 do
     begin
       if I = TRectangle(Sender).Tag then
       begin
         // TRectangle(Sender).Fill.Bitmap.Bitmap.CreateFromFile('D:\下载资源\icon\wx\16\tuichu.png');
          TRectangle(Sender).Fill.Bitmap.Bitmap := ImageList1.Bitmap(a,I-1);
          ft := findComponent('t'+I.ToString) As TText;
          ft.TextSettings.FontColor := TAlphaColorRec.Black;
       end
       else
       begin
          case I of
            1 : begin
                  r1.Fill.Bitmap.Bitmap := ImageList2.Bitmap(a,I-1);
                  t1.TextSettings.FontColor := TAlphaColorRec.Gray;
                end;
            2 : begin
                  r2.Fill.Bitmap.Bitmap := ImageList2.Bitmap(a,I-1);
                  t2.TextSettings.FontColor := TAlphaColorRec.Gray;
                end;
            3 : begin
                  r3.Fill.Bitmap.Bitmap := ImageList2.Bitmap(a,I-1);
                  t3.TextSettings.FontColor := TAlphaColorRec.Gray;
                end;
            4 : begin
                  r4.Fill.Bitmap.Bitmap := ImageList2.Bitmap(a,I-1);
                  t4.TextSettings.FontColor := TAlphaColorRec.Gray;
                end;
          end;
       end;

     end;

     frameShow(TRectangle(Sender).Tag);


end;


initialization
   //ownerFrm :=  TownerFrm.Create(nil);

finalization
  // if ownerFrm <> nil then
   //begin
    // ownerFrm.Parent := nil ;
   //  ownerFrm.Free;
   //end;
end.
